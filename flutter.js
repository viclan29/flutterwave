const Flutterwave = require('flutterwave-node-v3');
const flw = new Flutterwave(process.env.FLW_PUBLIC_KEY, process.env.FLW_SECRET_KEY);
const details = {
    account_bank: "044",
    account_number: "0690000040",
    amount: 200,
    currency: "NGN",
    narration: "Payment for things",
    reference: generateTransactionReference()+"_PMCKDU_1", // apending to allow for little delay
};
flw.Transfer.initiate(details)
    .then(console.log)
    .catch(console.log);

