const http = require("http");

const port = 1122;

const server = http.createServer(function(req, res){
	// define headers
	const headers = {
		'toktok' : 'Ata sitambui'
	};

	// write headers
	res.writeHead(200, headers);
	
	// write content
	res.write("Na hii ni content");

	// malizia sasa
	res.end();
})

// log
console.log("Starting server in port: " + port);

// listen
server.listen(port);
